package project.app.timediff;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    private Button btn;
    private TextView tv;
    private int state;
    private String start_time, end_time;
    private String format = "MM/dd/yyyy hh:mm a";
    private SimpleDateFormat sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.button);
        tv = findViewById(R.id.textView);
        state = 0;
        sdf = new SimpleDateFormat(format);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(state==0){
                    state = 1;
                    btn.setText("Stop");
                    start_time = sdf.format(new Date());
                    tv.setText(start_time);
                }
                else if(state==1){
                    state = 0;
                    btn.setText("Start");
                    end_time = sdf.format(new Date());

                    try {
                        Date dateObj1 = sdf.parse(start_time);

                        Date dateObj2 = sdf.parse(end_time);

                        long diff = dateObj2.getTime() - dateObj1.getTime();

                        int diffmin = (int) (diff / (60 * 1000));

                        double fare = (diffmin*20)/(30*1.0);

                        DecimalFormat fare_format = new DecimalFormat("#.##");

                        tv.setText(""+diffmin+" min\n"+fare_format.format(fare)+" tk");

                    } catch (ParseException e) {

                        tv.setText("Something wrong!");
                    }
                }

            }
        });

    }
}
